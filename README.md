Atividade 1 - FizzBuss
=============

Este algoritmo imprime números de 1 a 100. 
 
 - [x] para múltiplos de 3 é mostrado “Fizz” em vez do número
 - [x] para múltiplos de 5 é mostrado “Buzz”
 - [x] para números múltiplos de ambos (3 e 5), imprima “FizzBuzz”

## Instalação

1. Insira o arquivo "atividade-1.php" em seu servidor 
2. Configure o acesso ao arquivo em uma área de teste (ex.: localhost) 
2. Abra o arquivo em seu navegador
